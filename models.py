import enum

from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Enum, ForeignKey
from sqlalchemy import update
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Joueur(Base):
    __tablename__ = 'joueurs'
    id = Column(Integer, primary_key=True)
    nom = Column(String, nullable=False)
    prenom = Column(String, nullable=False)
    equipe = Column(ForeignKey("equipes.id"))

    def __init__(self, nom, prenom, equipeParam=None):
        self.id = None
        self.nom = nom
        self.prenom = prenom
        self.equipe = equipeParam

    def __str__(self):
        s = "Joueur - Id : {},Nom : {}, prenom {}".format(self.id, self.nom, self.prenom)
        if(self.equipe != None):
            s+=", Equipe : {}".format(self.equipe)
        return s

    def updateInDb(self):
        orm = ORM()
        orm.session.query(Joueur).filter(Joueur.id == self.id).\
            update({Joueur.nom: self.nom, Joueur.prenom: self.prenom, Joueur.equipe: self.equipe},synchronize_session=False)
        orm.session.commit()
        orm.closeConnexion()

    def insertInDb(self):
        orm = ORM()
        orm.session.add(self)
        orm.session.commit()
        orm.closeConnexion()

    @staticmethod
    def getOneById(id):
        orm = ORM()
        r = orm.session.query(Joueur).filter(Joueur.id == id)
        tabJoueur = []
        for e in r:
            tabJoueur.append(e)
        orm.closeConnexion()
        return tabJoueur

    @staticmethod
    def getAll():
        orm = ORM()
        r = orm.session.query(Joueur)
        tabJoueur = []
        for j in r:
            tabJoueur.append(j)
        return tabJoueur

        orm.closeConnexion()


class Equipe(Base):
    __tablename__ = "equipes"

    id = Column(Integer, primary_key=True)
    nom = Column(String, nullable=False)

    def __init__(self, nom):
        self.id = None
        self.nom = nom

    def __str__(self):
        s = "Equipe - Id : {}, Nom : {}".format(self.id,self.nom)
        return s

    def insertInDb(self):
        orm = ORM()
        orm.session.add(self)
        orm.session.commit()
        orm.closeConnexion()

    def updateInDb(self):
        orm = ORM()
        orm.session.query(Equipe).filter(Equipe.id == self.id). \
            update({Equipe.nom: self.nom},
                   synchronize_session=False)
        orm.session.commit()
        orm.closeConnexion()

    @staticmethod
    def getOneById(id):
        orm = ORM()
        r = orm.session.query(Equipe).filter(Equipe.id == id)
        tabEquipe = []
        for e in r:
            tabEquipe.append(e)
        orm.closeConnexion()
        return tabEquipe

    @staticmethod
    def getAll():
        orm = ORM()
        r = orm.session.query(Equipe)
        tabEquipe = []
        for e in r:
            tabEquipe.append(e)
        return tabEquipe

        orm.closeConnexion()

class Match(Base):
    __tablename__ = "matchs"

    id = Column(Integer, primary_key=True)
    lieu = Column(String, nullable=False)
    date = Column(DateTime, nullable=False)
    equipeDomicile = Column(ForeignKey("equipes.id"), nullable=False)
    equipeExterieur = Column(ForeignKey("equipes.id"), nullable=False)

    def __init__(self, lieu, date, equipeDomicileParam, equipeExterieurParam):
        self.id = None
        self.lieu = lieu
        self.date = date
        self.equipeDomicile = equipeDomicileParam
        self.equipeExterieur = equipeExterieurParam

    def __str__(self):
        s = "Match : Id {}, Lieu {}, date {}, equipe domicile {}, equipe exterieure {}".format(self.id, self.lieu, self.date, self.equipeDomicile, self.equipeExterieur)
        return s

    def insertInDb(self):
        orm = ORM()
        orm.session.add(self)
        orm.session.commit()
        orm.closeConnexion()

    def updateInDb(self):
        orm = ORM()
        orm.session.query(Match).filter(Match.id == self.id). \
            update({Match.lieu: self.lieu, Match.date: self.date, Match.equipeDomicile: self.equipeDomicile,
                    Match.equipeExterieur: self.equipeExterieur},
                   synchronize_session=False)
        orm.session.commit()
        orm.closeConnexion()

    @staticmethod
    def getOneById(id):
        orm = ORM()
        r = orm.session.query(Match).filter(Match.id == id)
        tabMatch = []
        for m in r:
            tabMatch.append(m)
        orm.closeConnexion()
        return tabMatch

    @staticmethod
    def getAll():
        orm = ORM()
        r = orm.session.query(Match)
        tabMatch = []
        for m in r:
            tabMatch.append(m)
        return tabMatch

        orm.closeConnexion()

class Tir(Base):
    __tablename__ = "tirs"

    id = Column(Integer, primary_key=True)
    joueur = Column(ForeignKey("joueurs.id"), nullable=False)
    match = Column(ForeignKey("matchs.id"), nullable=False)
    isBut = Column(Boolean, nullable=False)
    isPenalty = Column(Boolean, nullable=False)

    def __init__(self, joueurParam, matchParam, isBut=True, isPenalty=False):
        print("Init tir")
        self.id = None
        self.joueur = joueurParam
        self.match = matchParam
        self.isBut = isBut
        self.isPenalty = isPenalty

    def __str__(self):
        s = "Tir - id : {}, joueur : {}, match : {}, but : {}, penalty : {}".format(self.id, self.joueur, self.match,
                                                                                    self.isBut, self.isPenalty)
        return s
    def insertInDb(self):
        orm = ORM()
        orm.session.add(self)
        orm.session.commit()
        orm.closeConnexion()

class Expulsion(Base):
    __tablename__ = "expulsions"

    id = Column(Integer, primary_key=True)
    joueur = Column(ForeignKey("joueurs.id"), nullable=False)
    match = Column(ForeignKey("matchs.id"), nullable=False)
    nb = Column(Integer, nullable=False)
    type = Column(String, nullable=False)

    def __init__(self, joueurParam, matchParam, nb=1, type="cartonJaune"):
        self.id = None
        self.joueur = joueurParam
        self.match = matchParam
        self.nb = nb
        self.type = type

    def __str__(self):
        s = "Expulsion - id : {}, joueur : {}, match : {}, nb : {}, type : {}".format(self.id, self.joueur, self.match,
                                                                                    self.nb, self.type)
        return s

    def insertInDb(self):
        orm = ORM()
        orm.session.add(self)
        orm.session.commit()
        orm.closeConnexion()


class ORM:

    def __init__(self,dbName="databaseClean.db"):
        self.dbName = dbName
        self.session = None
        self.engine = None

        import os
        if not os.path.isfile(dbName):
            raise BDEError(
                "Connexion avec la base de données a échoué :\nErreur détectée : {} n'existe pas\n".format(dbName))
        try:
            self.engine = create_engine('sqlite:///' + dbName, echo=True)
            Session = sessionmaker(bind=self.engine)
            self.session = Session()
            # self.createOrReplaceDatabase()
            # print("Connexion BDD réussie")
        except Exception as err:
            raise err

    def closeConnexion(self):
        self.session.close()

    def createOrReplaceDatabase(self):
        print("Creation base")
        # Base.metadata.create_all(self.engine)
        print("Tables : ")
        d = {k: Base.metadata.tables[k].columns.keys() for k in Base.metadata.tables.keys()}
        print(d)

class BDEError(Exception) :
    def __init__(self,mess) :
        self.mess = mess
    def __str__(self) :
        return self.mess
