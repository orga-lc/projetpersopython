# coding: utf-8
import views.console.consoleView
import controllers
class EquipeConsoleView:

    def __init__(self):
        self.controllerEquipe = controllers.EquipeController()
        self.controllerJoueur = controllers.JoueurController()
        self.consoleView = views.console.consoleView.consoleView()

    def affichageMenuEquipe(self):
        print("--- GESTION DES EQUIPES ---")
        print("1 - Recherche equipe")
        print("2 - Ajout equipe")
        print("3 - Modification equipe")
        print("4 - Retour menu principal")

        choix = int(input("Votre choix ? : "))

        if choix == 1:
            self.rechercheEquipeMenu()
        elif choix == 2:
            self.ajoutEquipe()
        elif choix == 3:
            self.modificationEquipe()
        elif choix == 4:
            self.consoleView.affichageMenuPrincipal()
        else:
            self.affichageMenuEquipe()


    def rechercheEquipeMenu(self):
        print("--- RECHERCHE EQUIPE ---")
        print("1 - Par id")
        print("2 - Par nom")
        print("3 - Par joueur")
        print("4 - Retour menu precedent")
        choix = int(input("Votre choix ? : "))

        if choix == 1:
            self.rechercheEquipeById()
        elif choix == 2:
            self.rechercheEquipeByNom()
        elif choix == 3:
            self.rechercheEquipeByJoueur()
        elif choix == 4:
            self.affichageMenuEquipe()
        else:
            self.rechercheEquipeMenu()

    def rechercheEquipeById(self):
        val = int(input("id : "))
        tab = self.controllerEquipe.getByAttribute("id", val)
        for i in tab:
            print(i)
        self.rechercheEquipeMenu()

    def rechercheEquipeByNom(self):
        val = str(input("Nom : "))
        tab = self.controllerEquipe.getByAttribute("nom", val)
        for i in tab:
            print(i)
        self.rechercheEquipeMenu()

    def rechercheEquipeByJoueur(self):
        affichageListeJoueur = str(input("Voulez vous la liste des joueurs ? o/n : "))
        if (affichageListeJoueur == "o"):
            tabJoueur = self.controllerJoueur.getAll()
            for joueur in tabJoueur:
                print(joueur)
        val = int(input("id joueur : "))
        tab = self.controllerEquipe.getByAttribute("joueur",val)
        for i in tab:
            print(i)
        self.rechercheEquipeMenu()

    def ajoutEquipe(self):
        print("--- AJOUT EQUIPE ---")
        inputNom = str(input("Nom : "))

        self.controllerEquipe.createEquipe(inputNom)
        self.affichageMenuEquipe()

    def modificationEquipe(self):
        affichageListeEquipe = str(input("Voulez vous la liste des equipes ? o/n : "))
        if (affichageListeEquipe == "o"):
            tabEquipe = self.controllerEquipe.getAll()
            for equipe in tabEquipe:
                print(equipe)
        id = int(input("id equipe : "))
        nom = str(input("Nom : "))
        self.controllerEquipe.updateEquipe(id,nom)
        self.affichageMenuEquipe()


