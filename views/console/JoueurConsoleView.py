# coding: utf-8
import views.console.consoleView
import controllers
class JoueurConsoleView:

    def __init__(self):
        self.controllerEquipe = controllers.EquipeController()
        self.controllerJoueur = controllers.JoueurController()
        self.consoleView = views.console.consoleView.consoleView()

    def affichageMenuJoueur(self):
        print("--- GESTION DES JOUEURS ---")
        print("1 - Recherche joueur")
        print("2 - Ajout joueur")
        print("3 - Modification joueur")
        print("4 - Retour menu principal")

        choix = int(input("Votre choix ? : "))

        if choix == 1:
            self.rechercheJoueurMenu()
        elif choix == 2:
            self.ajoutJoueur()
        elif choix == 3:
            self.modificationJoueur()
        elif choix == 4:
            self.consoleView.affichageMenuPrincipal()
        else :
            self.affichageMenuJoueur()

    def rechercheJoueurMenu(self):
        print("--- RECHERCHE JOUEUR ---")
        print("1 - Par id")
        print("2 - Par nom")
        print("3 - Par prenom")
        print("4 - Par equipe")
        print("5 - Retour menu precedent")

        choix = int(input("Votre choix ? : "))

        if choix == 1:
            self.rechercheJoueurById()
        elif choix == 2:
            self.rechercheJoueurByNom()
        elif choix == 3:
            self.rechercheJoueurByPrenom()
        elif choix == 4:
            self.rechercheJoueurByEquipe()
        elif choix == 5:
            self.affichageMenuJoueur()
        else:
            self.rechercheJoueurMenu()

    def rechercheJoueurById(self):
        val = int(input("id : "))
        tab = self.controllerJoueur.getByAttribute("id", val)
        for i in tab:
            print(i)
        self.rechercheJoueurMenu()

    def rechercheJoueurByNom(self):
        val = str(input("Nom : "))
        tab = self.controllerJoueur.getByAttribute("nom", val)
        for i in tab:
            print(i)
        self.rechercheJoueurMenu()

    def rechercheJoueurByPrenom(self):
        val = str(input("Prenom : "))
        tab = self.controllerJoueur.getByAttribute("prenom", val)
        for i in tab:
            print(i)
        self.rechercheJoueurMenu()

    def rechercheJoueurByEquipe(self):
        affichageListeEquipe = str(input("Voulez vous la liste des equipes ? o/n : "))
        if (affichageListeEquipe == "o"):
            tabEquipe = self.controllerEquipe.getAll()
            for equipe in tabEquipe:
                print(equipe)
        val = int(input("id equipe : "))
        tab = self.controllerJoueur.getByAttribute("equipe", val)
        for i in tab:
            print(i)
        self.rechercheJoueurMenu()


    def modificationJoueur(self):
        print("--- Modification joueur ---")
        affichageListeJoueur = str(input("Voulez vous la liste des joueurs ? o/n : "))
        if (affichageListeJoueur == "o"):
            tabEquipe = self.controllerJoueur.getAll()
            for equipe in tabEquipe:
                print(equipe)
        id = int(input("id : "))
        nom = str(input("Nom : "))
        prenom = str(input("Prenom : "))
        affichageListeEquipe = str(input("Voulez vous la liste des equipes ? o/n : "))
        if (affichageListeEquipe == "o"):
            tabEquipe = self.controllerEquipe.getAll()
            for equipe in tabEquipe:
                print(equipe)
        equipe = int(input("id equipe : "))
        self.controllerJoueur.updateJoueur(id,nom,prenom,equipe)
        self.affichageMenuJoueur()




    def ajoutJoueur(self):
        print("--- Ajout joueur ---")
        inputNom = str(input("Nom : "))
        inputPrenom = str(input("Prenom : "))

        tabEquipe = self.controllerEquipe.getAll()
        for equipe in tabEquipe:
            print(equipe)
        inputIdEquipe = str(input("Id equipe : "))
        self.controllerJoueur.createJoueur(inputNom, inputPrenom, inputIdEquipe)
        self.affichageMenuJoueur()

