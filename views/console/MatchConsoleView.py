# coding: utf-8
import views.console.consoleView
import controllers
from datetime import date, datetime
class MatchConsoleView:

    def __init__(self):
        self.controllerEquipe = controllers.EquipeController()
        self.controllerJoueur = controllers.JoueurController()
        self.controllerMatch = controllers.MatchController()
        self.controllerTir = controllers.TirController()
        self.controllerExpulsion = controllers.ExpulsionController()
        self.consoleView = views.console.consoleView.consoleView()

    def affichageMenuMatch(self):
        print("--- GESTION DES MATCHS ---")
        print("1 - Recherche match")
        print("2 - Ajout match")
        print("3 - Modification match")
        print("4 - Récapituatif match")
        print("5 - Retour menu principal")

        choix = int(input("Votre choix ? : "))

        if choix == 1:
            self.rechercheMatchMenu()
        elif choix == 2:
            self.ajoutMatch()
        elif choix == 3:
            self.modificationMatchMenu()
        elif choix == 4:
            self.recapitulatifMatch()
        elif choix == 5:
            self.consoleView.affichageMenuPrincipal()
        else:
            self.affichageMenuJoueur()

    def rechercheMatchMenu(self):
        print("--- RECHERCHE MATCH ---")
        print("1 - Par id")
        print("2 - Par lieu")
        print("3 - Par date")
        print("4 - Par equipe")
        print("5 - Retour menu precedent")

        choix = int(input("Votre choix ? : "))

        if choix == 1:
            self.rechercheMatchById()
        elif choix == 2:
            self.rechercheMatchByLieu()
        elif choix == 3:
            self.rechercheMatchByDate()
        elif choix == 4:
            self.rechercheMatchByEquipe()
        elif choix == 5:
            self.affichageMenuMatch()
        else:
            self.rechercheMatchMenu()

    def rechercheMatchById(self):
        val = int(input("id : "))
        tab = self.controllerMatch.getByAttribute("id", val)
        for i in tab:
            print(i)
        self.rechercheMatchMenu()

    def recapitulatifMatch(self):
        affichageListeMatch = str(input("Voulez vous la liste des matchs ? o/n : "))
        if (affichageListeMatch == "o"):
            tabMatch = self.controllerMatch.getAll()
            for match in tabMatch:
                print(match)
        idMatch = int(input("id match : "))
        tab = self.controllerMatch.getByAttribute("id",idMatch)
        tabMatch = []
        for i in tab:
            tabMatch.append(i)

        print(tabMatch[0])
        tab = self.controllerTir.getByAttribute("match",tabMatch[0].id)
        for i in tab:
            print(i)

        self.affichageMenuMatch()


    def rechercheMatchByLieu(self):
        val = str(input("Lieu : "))
        tab = self.controllerMatch.getByAttribute("lieu", val)
        for i in tab:
            print(i)
        self.rechercheMatchMenu()

        # @TODO -> Date ne recherche pas correctement, revoir le parsage
    def rechercheMatchByDate(self):
        print("Format = JJ-MM-AAAA")
        strDate = str(input("Date : "))
        try:
            j = strDate.split("-")[0]
            m = strDate.split("-")[1]
            a = strDate.split("-")[2]

            val = datetime(int(a), int(m), int(j))
            tab = self.controllerMatch.getByAttribute("date", val)
            for i in tab:
                print(i)
        except:
            print("Erreur lors de la conversion de la date")
        self.rechercheMatchMenu()

    def rechercheMatchByEquipe(self):
        affichageListeEquipe = str(input("Voulez vous la liste des equipes ? o/n : "))
        if (affichageListeEquipe == "o"):
            tabEquipe = self.controllerEquipe.getAll()
            for equipe in tabEquipe:
                print(equipe)
        val = int(input("id equipe : "))
        tab = self.controllerMatch.getByAttribute("equipe", val)
        for i in tab:
            print(i)
        self.rechercheMatchMenu()

    def ajoutMatch(self):
        print("--- AJOUT MATCH ---")
        lieu = str(input("Lieu : "))
        print("Format = JJ-MM-AAAA")
        strDate = str(input("Date : "))
        try:
            j = strDate.split("-")[0]
            m = strDate.split("-")[1]
            a = strDate.split("-")[2]

            dateVal = datetime(int(a), int(m), int(j))
        except:
            print("Erreur lors de la conversion de la date")
            self.affichageMenuMatch()
        affichageListeEquipe = str(input("Voulez vous la liste des equipes ? o/n : "))
        if (affichageListeEquipe == "o"):
            tabEquipe = self.controllerEquipe.getAll()
            for equipe in tabEquipe:
                print(equipe)
        idDom = int(input("id equipe domicile: "))
        affichageListeEquipe = str(input("Voulez vous la liste des equipes ? o/n : "))
        if (affichageListeEquipe == "o"):
            tabEquipe = self.controllerEquipe.getAll()
            for equipe in tabEquipe:
                print(equipe)
        idExt = int(input("id equipe exterieure: "))
        self.controllerMatch.createMatch(lieu,dateVal,idDom,idExt)
        self.affichageMenuMatch()

    def modificationMatchMenu(self):
        print("--- MODIFICATION MATCH ---")
        print("1 - Modification lieu, date ou equipes")
        print("2 - Ajout buts")
        print("3 - Ajout expulsions")
        print("4 - Retour menu precedent")

        choix = int(input("Votre choix ? : "))

        if choix == 1:
            self.modificationMatch()
        elif choix == 2:
            self.ajoutButs()
        elif choix == 3:
            self.ajoutExpulsions()
        elif choix == 4:
            self.affichageMenuMatch()
        else:
            self.modificationMatchMenu()

    def modificationMatch(self):
        affichageListeMatch = str(input("Voulez vous la liste des matchs ? o/n : "))
        if (affichageListeMatch == "o"):
            tabMatch = self.controllerMatch.getAll()
            for match in tabMatch:
                print(match)
        id = int(input("id match : "))
        lieu = str(input("Lieu : "))
        print("Date modifiée a la date du jour")
        strDate = str(input("Date : "))
        try:
            j = strDate.split("-")[0]
            m = strDate.split("-")[1]
            a = strDate.split("-")[2]

            dateVal = datetime(int(a), int(m), int(j))
        except:
            print("Erreur lors de la conversion de la date")
            self.affichageMenuMatch()
        affichageListeEquipe = str(input("Voulez vous la liste des equipes ? o/n : "))
        if (affichageListeEquipe == "o"):
            tabEquipe = self.controllerEquipe.getAll()
            for equipe in tabEquipe:
                print(equipe)
        idEquipeDom = int(input("id equipe domicile: "))
        idEquipeExt = int(input("id equipe ext: "))


        self.controllerMatch.updateMatch(id,lieu,dateVal,idEquipeDom,idEquipeExt)
        self.affichageMenuMatch()

    def ajoutButs(self):
        affichageListeMatch = str(input("Voulez vous la liste des matchs ? o/n : "))
        if (affichageListeMatch == "o"):
            tabMatch = self.controllerMatch.getAll()
            for match in tabMatch:
                print(match)
        idMatch = int(input("id match : "))

        affichageListeJoueur = str(input("Voulez vous la liste des joueurs ? o/n : "))
        if (affichageListeJoueur == "o"):
            tabJoueur = self.controllerJoueur.getAll()
            for joueur in tabJoueur:
                print(joueur)
        idJoueur = int(input("id joueur : "))

        isBut = str(input("Tir marqué ? o/n : ")) == "o"
        isPenalty = str(input("Sur penalty ? o/n : ")) == "o"

        self.controllerTir.createTir(idJoueur, idMatch, isBut, isPenalty)
        self.modificationMatchMenu()

    def ajoutExpulsions(self):
        affichageListeMatch = str(input("Voulez vous la liste des matchs ? o/n : "))
        if (affichageListeMatch == "o"):
            tabMatch = self.controllerMatch.getAll()
            for match in tabMatch:
                print(match)
        idMatch = int(input("id match : "))

        affichageListeJoueur = str(input("Voulez vous la liste des joueurs ? o/n : "))
        if (affichageListeJoueur == "o"):
            tabJoueur = self.controllerJoueur.getAll()
            for joueur in tabJoueur:
                print(joueur)
        idJoueur = int(input("id joueur : "))

        nb = int(input("Nombre de faute : "))
        type = str(input("Type ? 2minutes/jaune/rouge : "))


        self.controllerExpulsion.createExpulsion(idJoueur, idMatch, nb, type)
        self.modificationMatchMenu()


