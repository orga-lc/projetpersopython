# coding: utf-8
from views.console import JoueurConsoleView, EquipeConsoleView, MatchConsoleView
import importCSV
import models
class consoleView():

    def affichageMenuPrincipal(self):

        print("--- GESTION D'UNE EQUIPE DE HANDBALL ---")
        print("1 - Gestion des joueurs")
        print("2 - Gestion des equipes")
        print("3 - Gestion des matchs")
        print("4 - Import des CSV")
        print("5 - Instanciation base")
        print("6 - Quitter")
        over = False
        while not over:
            choix = int(input("Votre choix ? : "))

            if choix == 1:
                joueurConsoleView = JoueurConsoleView.JoueurConsoleView()
                joueurConsoleView.affichageMenuJoueur()
            elif choix == 2:
                equipeConsoleView = EquipeConsoleView.EquipeConsoleView()
                equipeConsoleView.affichageMenuEquipe()
            elif choix == 3:
                matchConsoleView = MatchConsoleView.MatchConsoleView()
                matchConsoleView.affichageMenuMatch()
            elif choix == 4:
                self.importCSV()
            elif choix == 5:
                nomFichier = str(input("Nom du fichier : "))
                orm = models.ORM(nomFichier).createOrReplaceDatabase()
                # orm.createOrReplaceDatabase()
            elif choix == 6:
                over = True
                exit()
            else:
                self.affichageMenuPrincipal()


    def affichageMenuEquipe(self):
        print("--- GESTION DES EQUIPES ---")
        print("1 - Recherche équipes")
        print("2 - Ajout équipe")
        print("3 - Retour menu principal")
        over = False
        while not over:
            choix = int(input("Votre choix ? : "))

    def affichageMenuMatch(self):
        print("--- GESTION DES MATCH ---")
        print("1 - Recherche match")
        print("2 - Ajout match")
        print("Autre - Retour menu principal")
        over = False
        while not over:
            choix = int(input("Votre choix ? : "))

    def importCSV(self):
        importCSV.importCSV.importEquipe()
        importCSV.importCSV.importJoueur()
        importCSV.importCSV.importMatch()
        importCSV.importCSV.importTirs()