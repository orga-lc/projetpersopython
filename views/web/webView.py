from datetime import datetime

import cherrypy
import mako
import os.path
import controllers
import models

from mako.lookup import TemplateLookup
from mako.template import Template


class Main():


    def __init__(self):
        self.controllerJoueur = controllers.JoueurController()
        self.controllerEquipe = controllers.EquipeController()
        self.controllerMatch = controllers.MatchController()
        self.controllerTirs = controllers.TirController()
        self.mylookup = TemplateLookup(directories=[os.path.join(os.path.dirname(__file__), 'templates/')])

    @cherrypy.expose()
    def index(self):
        template = self.mylookup.get_template('choixEquipe.txt')
        tabEquipe = self.controllerEquipe.getAll()

        return template.render(equipes=tabEquipe)


    @cherrypy.expose()
    def dashboard(self, idEquipe=1):
        template = self.mylookup.get_template('index.txt')
        tabJoueurEquipe = self.controllerJoueur.getByAttribute("equipe", idEquipe)
        tabEquipe = self.controllerEquipe.getByAttribute("id", idEquipe)
        tabResumeMatch = self.createResume(idEquipe)
        strResumeStatsMatch = self.createResumeMatch(idEquipe)
        nomEquipe = "Equipe"

        print(strResumeStatsMatch)

        if(tabEquipe.__len__() != 0):
            nomEquipe = tabEquipe[0].nom

        return template.render(joueurs = tabJoueurEquipe,
                               nomEquipe = nomEquipe,
                               resumes = tabResumeMatch,
                               stats = strResumeStatsMatch)

    @cherrypy.expose()
    def detailJoueur(self, idJoueur=1):
        template = self.mylookup.get_template('detailJoueur.txt')
        joueur = self.controllerJoueur.getByAttribute("id", idJoueur)[0]
        equipe = self.controllerEquipe.getByAttribute("joueur", idJoueur)[0]
        infosJoueur = {'nom' : joueur.nom,
                       'prenom' : joueur.prenom,
                       'idEquipe' : equipe.id,
                       'equipe' : equipe.nom}
        buts = self.controllerTirs.getByAttribute("joueur", idJoueur).__len__()
        print(buts)
        infosJoueur['buts'] = buts

        matchs = self.controllerMatch.getMatchByJoueurAndEquipe(idJoueur, equipe.id).__len__()
        infosJoueur['matchs'] = matchs

        infosJoueur['butsParMatch'] = buts/matchs
        infosJoueur['cartonJaune'] = 0
        infosJoueur['expulsions'] = 0
        infosJoueur['cartonRouge'] = 0

        tabResumeMatch = self.createResume(equipe.id)
        strResumeStatsMatch = self.createResumeMatch(equipe.id)

        return template.render(infosJoueur = infosJoueur,
                               resumes=tabResumeMatch,
                               stats=strResumeStatsMatch
                               )

    @cherrypy.expose()
    def ajoutJoueur(self, nom="", prenom="", idEquipe=0):
        if(nom=="" and prenom == "" and idEquipe == 0):
            template = self.mylookup.get_template('ajoutJoueur.txt')
            tabEquipe = self.controllerEquipe.getAll()

            return template.render(equipes=tabEquipe)
        else:
            self.controllerJoueur.createJoueur(nom,prenom,idEquipe)
            raise cherrypy.HTTPRedirect('dashboard?idEquipe='+idEquipe)


    @cherrypy.expose()
    def ajoutMatch(self, idEquipeDom=0, idEquipeExt=0, lieu="", strDate=""):
        if(idEquipeDom==0 and idEquipeExt==0 and lieu=="" and strDate==""):
            template = self.mylookup.get_template("ajoutMatch.txt")
            tabEquipe = self.controllerEquipe.getAll()
            return  template.render(equipes=tabEquipe)

        else:
            print(idEquipeDom)
            print(idEquipeExt)
            j = strDate.split("-")[0]
            m = strDate.split("-")[1]
            a = strDate.split("-")[2]

            dateVal = datetime(int(a), int(m), int(j))
            print(dateVal)
            print(lieu)
            self.controllerMatch.createMatch(lieu, dateVal, idEquipeDom, idEquipeExt)
            raise cherrypy.HTTPRedirect('dashboard')



    def createResumeMatch(self,idEquipe):
        resume = {}
        tabMatch = self.controllerMatch.getByAttribute("equipe", idEquipe)

        chainesMatch = {}
        for m in tabMatch:
            joueursDom = self.controllerJoueur.getByAttribute("equipe",m.equipeDomicile)
            joueursExt = self.controllerJoueur.getByAttribute("equipe", m.equipeExterieur)
            l = 0;

            if joueursDom.__len__() >= joueursExt.__len__():
                l = joueursDom.__len__()
            else :
                l = joueursExt.__len__()
            s = ""
            for i in range(0, l):
                if i < joueursDom.__len__():
                    s+="{}-{}-0-".format(joueursDom[i].nom
                                         , self.controllerTirs.getByMatchAndJoueur(m.id, joueursDom[i].id).__len__())
                else :
                    s+=" - - -"
                if i < joueursExt.__len__():
                    s += "{}-{}-0_".format(joueursExt[i].nom
                                           , self.controllerTirs.getByMatchAndJoueur(m.id, joueursExt[i].id).__len__())
                else:
                    s += " - - _"
            chainesMatch[m.id] = s[:-1]
        return(chainesMatch)

    def createResume(self, idEquipe):
        tabJoueurAll = self.controllerJoueur.getAll()
        tabMatch = self.controllerMatch.getByAttribute("equipe", idEquipe)
        tabEquipe = self.controllerEquipe.getByAttribute("id", idEquipe)
        tabResumeMatch = []

        for m in tabMatch:
            nomEquipeDom = self.controllerEquipe.getByAttribute("id", m.equipeDomicile)[0].nom
            nomEquipeExt = self.controllerEquipe.getByAttribute("id", m.equipeExterieur)[0].nom
            resume = {m.equipeDomicile: 0,
                      m.equipeExterieur: 0}

            idEquipeNom = {m.equipeDomicile: nomEquipeDom,
                           m.equipeExterieur: nomEquipeExt}

            tirs = self.controllerTirs.getByAttribute("match", m.id)

            for t in tirs:
                for j in tabJoueurAll:
                    if t.joueur == j.id:
                        resume[j.equipe] += 1

            tabResumeMatch.append("{}_{}-{}-{}-{}".format(m.id, idEquipeNom[m.equipeDomicile], resume[m.equipeDomicile],
                                                       idEquipeNom[m.equipeExterieur], resume[m.equipeExterieur]))
        return tabResumeMatch