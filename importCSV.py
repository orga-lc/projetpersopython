import csv
import controllers
class importCSV:

    @staticmethod
    def importEquipe():
        controllerEquipe = controllers.EquipeController()
        with open('equipes.csv','rt',encoding='utf-8') as csvFile:
            reader = csv.DictReader(csvFile, delimiter=';')
            listeEquipe = list()
            for row in reader:
                controllerEquipe.createEquipe(row['nom'])
                print("Creation equipe : "+row["nom"])
                # print(e)

    @staticmethod
    def importJoueur():
        controllerJoueur = controllers.JoueurController()
        with open('joueurs.csv', 'rt', encoding='utf-8') as csvFile:
            reader = csv.DictReader(csvFile, delimiter=';')
            listeEquipe = list()
            for row in reader:
                controllerJoueur.createJoueur(row["nom"],row["prenom"],row["equipe"])
                print("Creation joueur : "+row["nom"])
                # print(j)

    @staticmethod
    def importMatch():
        from datetime import date
        controllerMatch = controllers.MatchController()
        with open('matchs.csv', 'rt', encoding='utf-8') as csvFile:
            reader = csv.DictReader(csvFile, delimiter=';')
            listeMatch = list()
            for row in reader:
                controllerMatch.createMatch(row["lieu"], date.today(), row["equipeDomicile"], row["equipeExterieur"])
                print("Creation match : "+row["lieu"])
                # print(m)

    @staticmethod
    def importTirs():
        controlleurTir = controllers.TirController()
        with open('tirs.csv', 'rt', encoding='utf-8') as csvFile:
            reader = csv.DictReader(csvFile, delimiter=';')
            listeMatch = list()
            for row in reader:
                controlleurTir.createTir(row["joueur"], row["match"], row["isbut"]=="True", row["ispenalty"]=="True")
                print("Creation tir : "+row["joueur"])
                # print(t)
