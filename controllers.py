from sqlalchemy import select,update, or_


import models
from sqlalchemy.sql.elements import and_


class JoueurController:

    def createJoueur(self,nom,prenom,equipe=None):
        j = models.Joueur(nom,prenom,equipe)
        j.insertInDb()
        return j

    def addEquipeToJoueur(self,idJoueur,idEquipe):
        print("fonction addEquipeToJoueur")
        equipe = models.Equipe.getOneById(idEquipe)
        if(len(equipe) == 1):
            joueur = models.Joueur.getOneById(idJoueur)
            if(len(joueur) == 1):
                print("id joueur existant")
                joueur[0].equipe = idEquipe
                joueur[0].updateInDb()


    def getByAttribute(self,clef,valeur):
        orm = models.ORM()
        result = []
        r = None
        if clef == "id":
            r = orm.session.query(models.Joueur).filter(models.Joueur.id == valeur)
        elif clef == "nom":
            r = orm.session.query(models.Joueur).filter(models.Joueur.nom == valeur)
        elif clef == "prenom":
            r = orm.session.query(models.Joueur).filter(models.Joueur.prenom == valeur)
        elif clef == "equipe":
            r = orm.session.query(models.Joueur).filter(models.Joueur.equipe == valeur)

        for j in r:
            result.append(j)

        orm.closeConnexion()
        return result

    def getAll(self):
        return models.Joueur.getAll()

    def updateJoueur(self, id, nom, prenom, equipe):
        j = models.Joueur.getOneById(id)
        if(len(j) > 0):
            joueur = j[0]
            joueur.nom = nom
            joueur.prenom = prenom
            joueur.equipe = equipe
            joueur.updateInDb()

class EquipeController:

    def createEquipe(self,nom):
        e = models.Equipe(nom)
        e.insertInDb()
        return e

    def getByAttribute(self, clef, valeur):
        orm = models.ORM()
        result = []
        r = None
        if clef == "id":
            r = orm.session.query(models.Equipe).filter(models.Equipe.id == valeur)
        elif clef == "nom":
            r = orm.session.query(models.Equipe).filter(models.Equipe.nom == valeur)
        elif clef == "joueur":
            rProv = orm.session.query(models.Joueur).filter(models.Joueur.id == valeur)
            prov = []
            for j in rProv:
                prov.append(j)
            r = orm.session.query(models.Equipe).filter(models.Equipe.id == rProv[0].equipe)


        for j in r:
            result.append(j)

        orm.closeConnexion()
        return result

    def getAll(self):
        return models.Equipe.getAll()

    def updateEquipe(self, id, nom):
        j = models.Equipe.getOneById(id)
        if (len(j) > 0):
            equipe = j[0]
            equipe.nom = nom
            equipe.updateInDb()




class MatchController:


    def createMatch(self,lieu,date,idEquipeDom, idEquipeExt):
        m = models.Match(lieu, date, idEquipeDom, idEquipeExt)
        m.insertInDb()
        return m

    def updateMatch(self, id, lieu, date, idEquipeDom, idEquipeExt):
        m = models.Match.getOneById(id)
        if (len(m) > 0):
            match = m[0]
            match.lieu = lieu
            match.date = date
            match.equipeDomicile = idEquipeDom
            match.equipeExterieur = idEquipeExt
            match.updateInDb()

    def getAll(self):
        return models.Match.getAll()

    def getByAttribute(self, clef, valeur):
        orm = models.ORM()
        result = []
        r = None
        if clef == "id":
            r = orm.session.query(models.Match).filter(models.Match.id == valeur)
        elif clef == "lieu":
            r = orm.session.query(models.Match).filter(models.Match.lieu == valeur)
        elif clef == "date":
            r = orm.session.query(models.Match).filter(models.Match.date == valeur)
        elif clef == "equipe":
            r = orm.session.query(models.Match).filter(or_(models.Match.equipeDomicile == valeur, models.Match.equipeExterieur == valeur))

        for j in r:
            result.append(j)

        orm.closeConnexion()
        return result
    def getMatchByJoueurAndEquipe(self, idJoueur,idEquipe):
        orm = models.ORM()
        result = []
        joueurController = JoueurController()
        joueurEquipeId = joueurController.getByAttribute("id",idJoueur)[0].equipe
        r = orm.session.query(models.Match).filter(or_(models.Match.equipeDomicile == idEquipe, models.Match.equipeExterieur == idEquipe))
        for m in r :
            if m.equipeDomicile == joueurEquipeId or m.equipeExterieur == joueurEquipeId:
                result.append(m)

        return result

class TirController():

    def createTir(self,idjoueur,idmatch,isbut,ispenalty):
        t = models.Tir(idjoueur, idmatch, isbut, ispenalty)
        t.insertInDb()
        return t

    def getByAttribute(self, clef, valeur):
        orm = models.ORM()
        result = []
        r = None
        if clef == "id":
            r = orm.session.query(models.Tir).filter(models.Tir.id == valeur)
        elif clef == "match":
            r = orm.session.query(models.Tir).filter(models.Tir.match == valeur)
        elif clef == "joueur":
            r = orm.session.query(models.Tir).filter(models.Tir.joueur == valeur)

        for j in r:
            result.append(j)

        orm.closeConnexion()
        return result

    def getByMatchAndJoueur(self, idMatch, idJoueur):
        orm = models.ORM()
        result = []
        r = None
        r = orm.session.query(models.Tir).filter(models.Tir.joueur == idJoueur, models.Tir.match == idMatch)
        for j in r:
            result.append(j)

        orm.closeConnexion()
        return result


class ExpulsionController():
    def createExpulsion(self, idjoueur, idmatch, nb, typeStr):
        ex = models.Expulsion(idjoueur, idmatch, nb, typeStr)
        ex.insertInDb()
        return ex

