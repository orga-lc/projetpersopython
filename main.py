# coding: utf-8
import views.console.consoleView
import views.web.webView
import cherrypy
import mako
import os.path
tutconf = os.path.join(os.path.dirname(__file__), 'web.conf')



if __name__ == "__main__":
    print("--- Demarage ----")
    # consoleview = views.console.consoleView.consoleView()
    # consoleview.affichageMenuPrincipal()
    cherrypy.quickstart(views.web.webView.Main(),'/',config=tutconf)