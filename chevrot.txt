Le but est de r�aliser un petit outil de suivi d'une �quipe de hand :
On veut stocker les informations permettant de savoir pour chaque match jou� de la saison le score,
qui a marqu� combien de points (dont x penalty), combien de sorties pour 3 minutes,
combien de penalty rat�s (par qui).
L'application doit vous permettre d'ajouter/supprimer un joueur, de saisir/supprimer les r�sultats d'un match,
de d'afficher divers �l�ments�:
par match (date, adversaire, lieu) le scores, le nombre de points marqu�s par joueur,
idem par match et par joueur, par joueur le nombre de points marqu�s (/match),
le nombre de "3 minutes", de penalty marqu�s/rat�s.
La page d'accueil donnera le r�sultat du dernier match saisi + stats.